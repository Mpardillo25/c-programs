#include <stdio.h>
#include <stdlib.h>

int main (){
   int num,fact;
   int i;
   printf ("Introduce el numero que quieres factorizar: ");
   scanf (" %i",&num);

   fact = 1;

   for (i=1; i<=num; i++)
	 fact *= i;
   
   printf ("El factorial %i! = %i\n", num, fact);

return 0;
}
