#include <stdio.h>
#include <stdlib.h>


int main () {

	int a = 5; //101
	int b = 8; //1000
	
	printf ("-------------Operaciones Lógicas-----------\n");

	int o = a^b; //1000+101 = 1101 --> 5+8 = 13
	printf ("%i ^ %i = %i\n",a,b,o);

        int u = a|b; //1000+101 = 1101 --> 5+8 = 13
        printf ("%i | %i = %i\n",a,b,u);

	int c = a&b; //0000
        printf ("%i & %i = %i\n",a,b,c);

        int d = !a; //010
	printf (" !%i = %i\n", a, d);


	return 0;
}
