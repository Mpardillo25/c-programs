#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
   char *frase = NULL;
   char letra;
   int leido = 0;

   printf ("Nombre: ");
   while ((letra = (char) getchar ()) != '\n') {
       frase = (char *) realloc (frase, ++leidos * sizeof(char));
       *(frase + leidos - 1) = letra;
       /*En este caso realloc funciona como malloc porque frase es  = NULL*/
       /*getchar  es igual scanf*/
     }
   frase = (char *) realloc (frase, ++leidos * sizeof(char));
   *(frase + leidos - 1) = '\0';
   
   printf ("Hola, %s\n", frase);

   free (frase);
 return EXIT_SUCCESS;
}
