#include <stdio.h>
#include <stdlib.h>


const char *lista[] = {
     "Hola",
     "Buenos días",
      NULL
};

int main (int argc, char *argv[]) {
    
    char frase[20];
    const char **p = lista; // **= lista = &lista[0]
    //int tam = sizeof(lista)/sizeof(char*);

    printf ("Escriba una palabra: ");
    scanf (" %s", frase); // no hace falta poner & porque es lo mismo que un array ya que guarda una direccion de memoria

    printf ("%s\n\n", frase);
    
   /*OTRA FORMA DE HACERLO:
    * for (int i = 0; i<tam; i++)
        printf (" %s", lista[i]);
      
    while (*(p++) != NULL)
	    printf (" %s\n", *p);
   */
    while (*p != NULL) {
       printf (" %s\n", *p);
       p++;
    }

    printf ("\n");
    printf (" sizeof(lista) --> %li bytes\n", sizeof(lista));
    printf (" sizeof(*lista) --> %li bytes\n", sizeof(*lista));
    printf (" sizeof(lista)/8 --> %li bytes\n", sizeof(lista)/sizeof(char*));
 return EXIT_SUCCESS;
}
