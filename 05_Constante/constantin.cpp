#include <stdio.h>

#define EURO 166.386

int main () {

   double input;

   printf ("How much do you wanna change? ");
   scanf (" %lf", &input);

   printf (" %.2lf pesetas => %.2lf€\n", input, input/EURO); /*EL %.2lf es para que imprima dos caracteres decimales de long float. Si se pusiese 4.2,
							       es para indicar que hay 4 caracteres  con 2 decimales*/
 
   return 0;
}
