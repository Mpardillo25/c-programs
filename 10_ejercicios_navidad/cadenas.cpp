#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXLIN 0x100

const char * const cprog = "cadenas";
char vprog[] = "programa";

void concatena (char *dest, const char *org, int max) {
  while (*dest != '\0') 
           dest ++;

  /*Versiones alternativas 
   * while (*(dest++) != '\0');
   * for (; *dest != '\0'; dest ++);
   * */

  /*Copiar car a car de org a dest cini nycho max o hasta \0*/

  for (int c=0; *org!='\0' && c<max; c++, ++org)
	  *dest = *org;
  *dest = *org;
 }
void cifra (char *frase, int clave) {
    while (*frase != '\0')
	    *(frase ++) += clave;
    /*
     * for(; *frase != '\0'; frase++)
     *    *frase += clave;
     * */

}

int main (int argc, char *argv[]) {
    char linea [MAXLIN];
    strcpy (linea,cprog);
    strncpy (linea,cprog, MAXLIN);
    strcat (linea, " ");
    strncat (linea, vprog, MAXLIN - strlen(linea));

    concatena (linea, "Esto es un mensaje secreto", MAXLIN - strlen(linea));

    cifra (linea + strlen(linea) + 1,3);

    printf ("%s\n", linea);
    printf ("Secreto: %s\n",linea + strlen(linea) + 1);



 return EXIT_SUCCESS;
}
