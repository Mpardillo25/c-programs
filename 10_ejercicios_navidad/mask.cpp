#include <stdio_ext.h>
#include <stdlib.h>
#include <stdio.h>

#define COLS 3
#define R 2
#define G 1
#define B 0

unsigned char pedir_color (const char *nombre) {
  int buffer;
  
  printf ("\n");
  do {
      printf ("\x1B[1A");
      printf ("%s (0-255): ", nombre);
      printf ("            ");
      printf("\x1B[10D");
      __fpurge(stdin);
      scanf (" %u", &buffer);
  }while (buffer < 0 || buffer > 255);

  return (unsigned char) buffer;
}


int main (int argc, char *argv[]) {
    unsigned char rgb[COLS],
                  mask[COLS];
    rgb[R] = pedir_color ("Rojo");
    rgb[G] = pedir_color ("Verde");
    rgb[B] = pedir_color ("Azul");
 return EXIT_SUCCESS;
}
