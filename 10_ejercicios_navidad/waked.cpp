/*3210
1001 => 9
PAPUP => 8 1000
MAMUP => 4 0100
MUMUP => 2 0010
HIJAP => 1 0001

waked = PAPUD | HIJAP;
waked = waked | PAPUP; => Poner el padre a despierto
waked = waked ^ PAPUP; => Conmutar al padre
waked = waked & ~PAPUP; => Acostar al padre */


#include <stdio.h>
#include <stdlib.h>

#define PAPUP 8
#define MAMUP 4
#define HIJOP 2
#define HIJAP 1

enum TMiembro {
    papa,
    mama,
    hijo,
    hija,
    total_miembros
};

/*Conmutar: devuelve el valor ols_status habiendo cambiado el valor del bit correspondiente al miembro de la familia*/
int conmutar (int old_status, enum TMiembro miembro){

    int new_status;
    int bit;

    switch (miembro) {
         case papa:
		 bit = PAPUP;
              break;
	 case mama:
	        bit = MAMUP;
	      break;
	 case hijo:
	        bit = HIJOP;
	      break;
	 case hija:
	        bit = HIJAP;
	      break;
         default:
	      bit = 0;
    }
    new_status = old_status ^ bit;

    return new_status;
}

/*Acostar: devuelve el valor old_status habiéndo puesto a 0 el bit correspondiente al miembro de la familia*/
int acostar (int old_status, enum TMiembro miembro){
   int bit = 1,
       pos = total_miembros - 1 - miembro; //Invertimos los miembros de la familia
     
   for (int vez=0; vez<pos; vez ++)
	   bit = bit << 1;
  
  
   return old_status & ~bit;
}

/*Levantar: devuelve el valor old_status habiéndo levantado el bit correspondiente al miembro de la familia*/
int levantar (int old_status, enum TMiembro levantado) {
   int bit = 1,
       pos = total_miembros - 1 - levantado;

    for(int vez=0; vez<pos; vez ++)
	    bit = bit << 1;

  return old_status | bit;
}

/*Imprime con texto el estado de cada miembro de la familia*/
void print (int waked) {
    printf ("Papá esta %s\n", waked & PAPUP ? "levantado" : "acostado");
}

int main () {
	int waked = 0;
	waked = levantar (waked,papa);
	waked = acostar (waked,papa);
	waked = conmutar (waked,papa);

	print (waked);

 return EXIT_SUCCESS;
}
