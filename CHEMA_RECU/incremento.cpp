#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 6 //define variable n como 6
#define M 20 //define variable m como 20

void muestra (int a[N]) { //funcion muestra contiene un entero de a de dimension n, es decir 6
    for (int i=0; i<N; i++) //para entero i es 0, menor que n o 6 y sumar uno al entero i
        printf ("%i ", a[i]); //Imprimir por pantalla el entero i, de la variable a de dimension i
    printf ("\n");//salto linea
}

int main (int argc, char *argv[]) {
    int A[N]; //entero a de dimension n, es decir 6
    int menor; //entero llamado menor

    /* Inicialización */
    srand (time (NULL)); //secuencia nueva de numeros pseudoaleatorios para se retornados por llamadas posteriores a rand.
    for (int i=0; i<N; i++) //para entero i es 0, menor que 6 que es N y se suma uno a la i
        A[i] = rand () % M + 1;   // Números entre 1 y 20

    muestra (A); //funcion muestra
    /* Ordenación de inserción */

    for (int buscando=0; buscando<N-1; buscando++) { //para entero buscando es 0, buscando tiene que ser menor que 6-1 e incrementarle en uno
        menor = buscando; //menor es igual que buscando
        for (int i=buscando+1; i<N; i++) //para entero i es buscando + 1, i es menor que 6 y se incrementa en uno
            if (A[i] < A[menor]) //si a de dimension i es menor a A de dimension menor
                menor = i; //menor sera igual a i si se cumple
        if (menor > buscando) { //si menor es mayor que buscando
            int aux = A[buscando]; //entero aux es igual a A de dimension buscando
            A[buscando] = A[menor]; //A que contiene el valor de buscando sera igual a A  que contiene el valor de menor 
            A[menor] = aux; //y el valor de menor en a sera igual a aux
        }
    }
    /* Fin de la ordenación */
    muestra (A);


    return EXIT_SUCCESS;
}

