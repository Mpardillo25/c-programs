#include <stdio.h>
#include <stdlib.h>

#define TECNICO 1
#define SOCIALES 2
#define CONTACTO 4

enum TipoPalo{oros, copas, espadas, bastos, PALOS};   /* Con enum vamos a
            * definir o enumerar los distintos datos que hay, en los cuales
            * oros vale 0, copas vale 1, espadas vale 2, bastos vale 3, y
            * PALOS vale cuatro. PALOS sirve para contabilizar cuántos hay.
            * Si quieres cambiar el valor de uno de los datos, por ejemplo,
            * pones espadas=3, y esto haría que el siguiente fuese 4, y el
            * siguiente 5, etc... */

int main (int argc, char *argv[]) {

    enum TipoPalo micarta = espadas; /* defino una variable de la enumeración
            * de TipoPalo, escogiendo uno de los valores de ella. Es decir,
            * le asigno a la variable micarta el valor 2 -que es el valor
            * que tiene espadas en la enumeración TipoPalo-. */

    int mariano = SOCIALES | CONTACTO;  /* Defino una variable de tipo int
            * ¡OJO! con el valor del define SOCIALES ó(+) el valor del define
            * CONTACTO. Es decir, 0010 ó 0100 es 0110, es decir, 6. */

    int usuario;    /* Defino una variable de tipo int para usarla luego
                     * con un scanf.  */

    printf ("Escoja un valor entre 0 y 9 (puedes probar el 6): ");
    scanf (" %i", &usuario);    /* Con este scanf, solicito un valor al
                                 * usuario.  */

    if (mariano == usuario)     /* Si el valor de mariano -que recordemos,
                         * es la operación ó de 0010 (2) y 0100 (4),
                         * es decir, 0110 (6) es igual al valor que ha
                         * introducido el usuario...  */
        printf ("El valor de enum TipoPalo es %d\n", micarta); /* Se imprime
                         * el valor numérico de micarta, que corresponde
                         * a un valor de TipoPalo, espadas, en la posición 2.  */
    else
        printf ("No se  ha guardado un resultado para enum TipoPalo.\n");



    return EXIT_SUCCESS;
}

