#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    int base;

    printf ("Vamos a crear una sucesión triangular de asteriscos.\n");
    printf ("Indíqueme cuál quiere que sea el tamaño de la base: ");
    scanf (" %i", &base);     /* Guardo el valor dado por el usuario
                  * en la dirección de memoria de base  */
    printf ("\n");

    if (base<=1) {
        printf ("El número que ha indicado es muy pequeño.\n");
    } else {
        for (int altura=0; altura<base; altura++){
            for (int lado=0; lado<altura+1; lado++)
                 printf("✱");
            printf("\n");
        }
    }

    return EXIT_SUCCESS;
}

