/******************************EJERCICIO REPASO*************
 * Hacer un programa en el cual podamos introducir todos los numeros que queramo sy después hacer la media de esos numeros
 * ***********************************************************/

#include <stdio.h>
#include <stdlib.h>

int main (){
   int x,i,y;
   float suma; //contador

   printf ("Introduce cuantós numeros quieres calcular la media: ");
   scanf (" %i", &x);

   i = 0;
   suma=0;
   while (i < x){
      
	   printf ("Introduce el %i numero: ", i);
	   scanf (" %i", &y);
	   suma += y;
	   i++;
   }
   suma = suma/x;
   printf ("El resultado de la media de todos los numeros introducidos es: %f \n",suma);

 return EXIT_SUCCESS;
}
