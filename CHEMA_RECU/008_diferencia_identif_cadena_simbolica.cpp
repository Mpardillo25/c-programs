#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    unsigned a = 0x61;          // Lo primero que vamos a hacer es crear
    char identif_entero = a;    /* un identificador entero sin signo, con el valor
                                 * hexadecimal de la letra a. Luego lo vamos a
                                 * asignar a una variable de tipo carácter. ¿Por qué
                                 * hacemos esto? Para comprobar todas las opciones. */

    char identif_hexa = 0x61;   /* Ahora, creamos un identifcador de tipo char, asignándole
                                 * el valor hexadecimal de a, que es 0x61. */

    char *cadena = "a";         /* Creamos un string que contenga el valor a. Esto se
                                 * consigue mediante el tipo char *variable, y el valor
                                 * entre comillas dobles. */

    char simbolica = 'a';       /* Ahora creamos un identificador de tipo char, con una
                                 * constante simbólica. Esto lo conseguimos poniendo el
                                 * valor entre comillas simples. */

    printf ("¿Qué valores tendrán las siguientes expresiones?\n\n");

    printf ("unsigned a = 0x61;\n");
    printf ("char identif_entero = a;\n");
    printf ("Este es el carácter de char identif_entero = %c\n", identif_entero);
    printf ("Este es el valor entero de char indentif_entero = %i\n", identif_entero);
    printf ("Este es el valor hexadecimal de char identif_entero = %x\n\n", identif_entero);

    printf ("char identif_hexa = 0x61;\n");
    printf ("Este es el carácter de char identif_hexa = %c\n", identif_hexa);
    printf ("Este es el valor entero de char indentif_hexa = %i\n", identif_hexa);
    printf ("Este es el valor hexadecimal de char identif_hexa = %x\n\n", identif_hexa);

    printf ("char *cadena = \"a\";\n");
    printf ("Este es el carácter de char  cadena = %s\n", cadena);
    printf ("Este es el valor entero de char cadena = %i\n", cadena);
    printf ("Este es el valor hexadecimal de char cadena = %x\n\n", cadena);

    printf ("char simbolica = 'a'\n");
    printf ("Este es el carácter de char simbolica = %c\n", simbolica);
    printf ("Este es el valor entero de char simbolica = %i\n", simbolica);
    printf ("Este es el valor hexadecimal de char simbolica =  %x\n", simbolica);

    return EXIT_SUCCESS;
}

