#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main (int argc, char *argv[]){

  char nombre [20] = "Maria";
  char apellido[20] = "Pardillo Pérez";

  /*STRCPY --> SOBREESCRIBE /COPIA*/
//  strcpy (apellido,nombre); //copia lo que hay en nombre lo sobreescribe en apellido.
  printf (" %s\n", apellido); //Maria
  
  /*STRLEN --> CUENTA LOS CARACTERES*/
 int long1, long2;
  long1=strlen(nombre);
  
  printf ("La longuitud de las dos cadenas juntas es de %i\n", long1);


  /*STRCAT--> CONCATENA*/

  strcat (nombre, " ");
  strcat (nombre, apellido);
  strcat (nombre, " ");
  strcat (nombre, "Encantada de conocerte");

  printf (" %s\n",nombre);
return EXIT_SUCCESS;
}
