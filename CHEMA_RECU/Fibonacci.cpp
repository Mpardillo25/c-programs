/*Consiste en una sucesión de números en el que empieza  en 0 y 1 y se va sumando los dos numeros anteriores
 *
 *  0 1 1 2 3 5 8 13 21...
 *
 * */


#include <stdio.h>
#include <stdlib.h>

void title () {
 system ("clear");
 system ("toilet -fpagga --gay F I B O N A C C I");
}

int main () {
    title ();
    
    int x,y,resultado;
    int n;
    int cont; //iniciliazimos el cont a 3 para que salgan los numeros que queremos que haga sucesion ya que el 0 y dl 1 ya están metidos y no lo cuenta

   printf ("¿De cuántos numeros quieres hacer la sucesión: ");
   scanf (" %i", &n);
   
   cont = 3;
   x= 0;
   y= 1; 

   printf (" 0, 1, ");
   while (cont <= n){
     resultado = x + y;
      
     printf ("%i, ", resultado);

     x = y; //x pasa a valer 1 
     y = resultado; // y pasa a valer 1
     cont++;
   }
   printf ("\n");

return EXIT_SUCCESS;
}
