#include <stdio.h>

int main() {

    char letra;   // Declaramos una variable de tipo char

    for (letra = 0x63; letra >= 0x43; letra--)        // Con este for decimos
        printf("letra en mayúsculas %c\n", letra);    /* que recorra desde
                              *  la letra 0x63 -que es la c, hasta la letra
                              *  0x43 -que es la C-  y va imprimiendo todos
                              *  los caracteres.  */

    return 0;
}

