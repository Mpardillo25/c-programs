#include <stdio.h>
#include <unistd.h>

#define MAXCOLUMNAS 100     /* Utilizamos el define para indicarle que
                  * el valor de columnas máximo que va a utlizar es 100.
                  * En verdad no es necesario, salvo para modificar
                  * fácilmente el número de columnas */

int main () {

    /* En los siguientes tres printf, tenemors la misma línea escrita
     * de tres formas distintas */
    printf ("Hola \x0A \x09 Beep \x07 \x0A");

    printf ("Hola \012 \011 Beep \007 \012");

    printf ("Hola \n    \t  Beep  \a   \n");

    /* Es decir:
     *                      Salto de línea    Tabulación    Beep
     * hexadecimal              \x0A            \x09        \x07
     * octal                    \012            \011        \007
     * secuencia de escape      \n              \t          \a   */

    printf ("Hola \n \0 esto es secreto"); /* En este printf nos
          * encontramos la secuencia de escape \0. Lo que esto hace es
          * hacer entender al compilador  que es la marca de fin de cadena,
          * -es decir, que no hay nada más escrito-  y todo lo que hay detrás
          *  de \0 no va a aparecer. ¡OJO! Esto va a dar un warning al
          *  compilar, pero sí es posible ejecutarlo. */

    printf ("\n");

    for (int vez=0; vez<MAXCOLUMNAS; vez++) {
        printf ("\r");    /* La secuencia de escape \r, lo que hace es
              * volver al principio de la línea. Es decir, retorno de
              * carro. Así, sobreescribimos la línea actual, creando
              * una línea de estado "creciente" con cada incremento. */

        for (int igual=0; igual<vez; igual ++)
            printf ("=");   /* Imprimimos la línea de estado con el
                * valor que tenga vez, que en cada turno será uno más.  */

        printf("> %2i\%%", vez);    /* Imprimimos el contador percentual,
                * con el valor de vez representado como dos números.  */

        fflush (stdout);    /* fflush es usado típicamente para tubos de
                * salida. Su propósito es limpiar el buffer de salida y
                * mover los datos a consola.  */

        usleep (1000000);   /* usleep lo que hace es suspender la ejecución
                * del programa en microsegundos. Básicamente, es como
                * crear un reloj o contador de tiempo.  */
    }

    printf("\n.FIN.");

    return 0;
}

