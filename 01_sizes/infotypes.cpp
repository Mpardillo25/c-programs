//01_.Imprimir el tamaño de las diferetes variables


#include <stdio.h>

int main () {
  long int var_longint;
  short int var_shortint;
  signed int var_signed;
  long long var_2longint;
  long double var_longd;

  printf ("long int: %lu bytes \n\a", sizeof(var_longint));
   printf ("long long int: %ld bytes \n\a", sizeof(var_2longint));
    printf ("short int: %lu bytes \n\a", sizeof(var_shortint));
     printf ("signed int: %lu bytes \n\a", sizeof(var_signed));
   
   printf ("int: %lu bytes \n\a", sizeof (int));
    printf ("char: %lu bytes \n\a", sizeof (char));
     printf ("float: %lu bytes \n\a", sizeof (float));
      printf ("long double: %lu bytes \n\a", sizeof (var_longd));
       printf ("double: %lu bytes \n\a", sizeof (double));
  
  return 0;
}
