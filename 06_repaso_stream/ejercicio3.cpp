#include <stdio.h>
#include <stdlib.h>

/* const char *carta = "";
-- Con el const no deja cambiar la variable carta ya que es una constante y no cambia, para que no se haga una cadena mas larda y se cargue el programa.
-- Simpre que ponga una variable tipo char de varios caracteres tendré que poner * antes de la variable
*/
int main (int argc, char *argv[]) {
	char carta [] = ""; //creamos mejor un array en vez de *carta porque viola al segmento

	//char final = carta + 3;
	char *fin;
	fin = carta + 3;
	++(*fin); //con *carta++ no se puede solo hacer eso porque si no avanzamos a la ultima direccion pero no sabemos volver po eso se pone un *fin, para que podamos saber volver al imprimir la carta
	// *carta++; con *carta++ suma 1 la primera direccion de memoria, incrementa lo que tiene carta y con carta++ pasa a la siguiente direccion de la cadena del caracter por ejemplo, Hola seria ola.
	// Se puede usar carta[3] por ejemplo para ir a direcciones de emoria y no hace falta tener otra variable para volver.(*fin)
	// Otra forma de hacerlo es ++(*(carta +3)); se llama "connotacion de puntero" ya que se pone todo en una linea.
	// El segundo paso se pone char quitando el const de arriba  y se pone abajo de int main como si fueras una variable local
	printf ("%s\n", carta); /*Suministra direcciones de memoria con %s byte  byte*/

 return EXIT_SUCCESS;
}
