#include<stdio.h>
#include<unistd.h>

//Ver lo que hace la expresion \r y observar lo que hace en gdb en la salida de la funcion fflush(stdout)
int main (){
  int numero = 0;
  printf ("%i\n", numero);

  for (numero = 0; numero<100; numero ++){
      printf ("\r%i", numero);
      fflush (stdout); 
  }
  printf ("\n");

 return 0;
}
