#include<stdio.h>
#define SALTO ( 'a' - 'A' )

int main(){
    
    char letra, nueva_letra;
    printf ("Introduce una letra para pasar a minúscula/mayúscula\n");
    scanf("%c",&letra);    /*Llamamos a la funcion scanf para guardar el valor de la letra en una posicion de memoria &*/
    
    /*Con la condicional if, si el caracter itroducido es una mayuscula o una minuscula*/

    if (letra >= 0x61 && letra <= 0x7A){ //si letra es mayor o igual que 'a' (hexadecimal: 0x61) y menor que la 'z'(hexa: 0x7A)
      
         nueva_letra = letra - SALTO;
         printf ("'%c' de  minúscula --> '%c' mayúscula\n", letra, nueva_letra);
     }
       else if( letra <= 0x41 && letra <= 0x5A ) { //si la letra es menor o igual que 'A' y menor o igual que 'Z'
             nueva_letra = letra + SALTO; //aquí pasamos la letra mayúscula introducida a minúscula ya que si le sumamos 0x20 se convierte en minuscula.
	     printf ("'%c'de mayúscula --> '%c' minúscula\n", letra, nueva_letra);
       }
    else{
        printf ("%c no es una letra, vuelva a introducir los datos correctamente, Gracias!",letra); 
       }
 	 
 return 0;
}
