#include <stdio.h>
#include <stdlib.h>

#ifndef NUM
#define NUM 3 /*Al ser impar el numero en la condicion nos va a dar una 'i', mientras que si ponemos NUM 2 al ser un numero par y resto 0 pone una 'p'*/
#endif

int main (int argc, char *argv[]) {
    char resultado = NUM % 2 == 0 ? 'p' : 'i'; //si NUM da de resto 0 al dividirlo entre 2 entonces pone un 'p' y si no es de resto 0 una 'i'
    printf ("%i => %c\n", NUM, resultado);

 return EXIT_SUCCESS;
}
