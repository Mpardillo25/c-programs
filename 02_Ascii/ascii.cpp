//02_.Imprimir los caracteres ASCII

#include<stdio.h>
#define LOW 0X20
#define HIGH 0X7E

int main(){
    
	signed char i;
	for(i=LOW; i<=HIGH; i++)
	   printf ("%X = %c\n", i, i);

}
